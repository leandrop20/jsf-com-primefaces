package br.com.devmedia.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="autoCompleteBeanAvancado")
@SessionScoped
public class AutoCompleteBeanAvancado {

	private List<String> contatos;
	private List<String> contatosSelecionados;
	
	public AutoCompleteBeanAvancado() {
		contatos = new ArrayList<String>();
		
		contatos.add("test@gmail.com");
		contatos.add("test2@gmail.com");
		contatos.add("test3@gmail.com");
		contatos.add("test4@gmail.com");
		contatos.add("test5@gmail.com");
	}
	
	public List<String> complete(String busca) {
		List<String> resultados = new ArrayList<String>();
		for (String contato : contatos) {
			if (contato.toUpperCase().contains(busca.toUpperCase())) {
				resultados.add(contato);
			}
		}
		return resultados;
	}

	public List<String> getContatosSelecionados() {
		return contatosSelecionados;
	}

	public void setContatosSelecionados(List<String> contatosSelecionados) {
		this.contatosSelecionados = contatosSelecionados;
	}
	
	
	
}
