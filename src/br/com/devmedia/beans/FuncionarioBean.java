package br.com.devmedia.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="funcionarioBean")
@RequestScoped
public class FuncionarioBean {

	private String nome;
	private String funcao;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getFuncao() {
		return funcao;
	}
	
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	
	public void salvar() {
		FacesContext.getCurrentInstance().addMessage(
			null,
			new FacesMessage(
				"Ol�" + nome + ", voc� trabalha como " + funcao + ";")
		);
		
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
}
