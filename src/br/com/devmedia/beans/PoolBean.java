package br.com.devmedia.beans;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="poolBean")
@SessionScoped
public class PoolBean {

	private int count;
	private Date data;
	
	public PoolBean() {
		data = new Date();
	}
	
	public void incrementar() {
		count++;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getData() {
		data = new Date();
		return data;
	}
	
}
