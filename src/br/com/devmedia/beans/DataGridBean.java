package br.com.devmedia.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

@ManagedBean(name="dataGridBean")
@SessionScoped
public class DataGridBean {

	private List<Produto> produtos;
	private SelectItem[] categorias;
	private Produto produtoSelecionado;
	private Produto[] produtosSelecionados;
	
	public DataGridBean() {
		produtos = new ArrayList<Produto>();
		
		produtos.add(new Produto(1, "PS3", 999, 20, "Console"));
		produtos.add(new Produto(2, "XBOX", 799, 180, "Console"));
		produtos.add(new Produto(3, "WII", 1699, 150, "Console"));
		produtos.add(new Produto(4, "IPhone", 2199, 300, "Mobile/Tablet"));
		produtos.add(new Produto(5, "Galaxy S3", 1799, 20, "Mobile/Tablet"));
		produtos.add(new Produto(6, "IPad 3", 2398, 60, "Mobile/Tablet"));
		produtos.add(new Produto(7, "IPhone 5", 2999, 70, "Mobile/Tablet"));
		produtos.add(new Produto(8, "Notebook DELL", 2100, 130, "Notebook"));
		produtos.add(new Produto(9, "Notebook Sony", 1800, 150, "Notebook"));
		produtos.add(new Produto(10, "Notebook Positivo", 1300, 210, "Notebook"));
		
		categorias = new SelectItem[4];
		categorias[0] = new SelectItem("", "Selecione");
		categorias[1] = new SelectItem("Console", "Console");
		categorias[2] = new SelectItem("Mobile/Tablet", "Mobile/Tablet");
		categorias[3] = new SelectItem("Notebook", "Notebook");
	}
	
	public void onRowSelect(SelectEvent event) {
		FacesMessage msg = new FacesMessage(
			"Produto Selecionado",
			((Produto) event.getObject()).getNome()
		);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public void onRowUnselect(UnselectEvent event) {
		FacesMessage msg = new FacesMessage(
			"Sele��o removida",
			((Produto) event.getObject()).getNome()
		);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public SelectItem[] getCategorias() {
		return categorias;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}

	public Produto[] getProdutosSelecionados() {
		return produtosSelecionados;
	}

	public void setProdutosSelecionados(Produto[] produtosSelecionados) {
		this.produtosSelecionados = produtosSelecionados;
	}
	
}
