package br.com.devmedia.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ManagedBean(name="clienteBean")
@SessionScoped
public class ClienteBean {

	private String nome;
	private String cargo;
	
	public ClienteBean() {
		
	}
	
	public void inserir(ActionEvent event) {
		try {
			Thread.sleep(3000);
			FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage("Cliente inserido com sucesso!")
			);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
}
