package br.com.devmedia.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.DragDropEvent;

@ManagedBean(name="carroBean")
@SessionScoped
public class CarroBean {

	private List<Carro> carros;
	private List<Carro> selecionados;
	
	public CarroBean() {
		carros = new ArrayList<Carro>();
		
		carros.add(new Carro("Classic", 2010, "Ar"));
		carros.add(new Carro("Cobalt", 2013, "Completo"));
		carros.add(new Carro("Gol", 2012, "Ar, Dire��o"));
		carros.add(new Carro("Fiat", 2011, "B�sico"));
		carros.add(new Carro("Peugeout", 2012, "Ar, Dire��o, Vidro"));
		carros.add(new Carro("Celta", 2009, "Dire��o"));
		carros.add(new Carro("Hilux", 2008, "Completo"));
		carros.add(new Carro("Corolla", 2005, "Completo"));
		
		selecionados = new ArrayList<Carro>();
	}
	
	public void onSelecionado(DragDropEvent event) {
		Carro carro = (Carro) event.getData();
		carros.remove(carro);
		selecionados.add(carro);
	}

	public List<Carro> getCarros() {
		return carros;
	}

	public List<Carro> getSelecionados() {
		return selecionados;
	}
	
}
