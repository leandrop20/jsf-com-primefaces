package br.com.devmedia.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ManagedBean(name="contextMenuGalleryBean")
@RequestScoped
public class ContextMenuGalleryBean {

	public void enviar(ActionEvent actionEvent) {
		adicionarMensagem("Envio realizado com sucesso!");
	}

	private void adicionarMensagem(String mensagem) {
		FacesMessage msg = new FacesMessage(
			FacesMessage.SEVERITY_INFO,
			mensagem,
			null
		);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	
	
}
