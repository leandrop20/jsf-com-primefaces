package br.com.devmedia.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

@ManagedBean(name="loginBean")
@SessionScoped
public class LoginBean {

	private String name;
	private String password;
	
	public LoginBean() {
		
	}
	
	public void login(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage msg = null;
		
		boolean logado = false;
		
		if (name != null && name.equals("admin")
				&& password != null && password.equals("devmedia")) {
			logado = true;
			msg = new FacesMessage(
				FacesMessage.SEVERITY_INFO,
				"Seja Bem-vindo",
				"Seja bem-vindo " + name);
		} else {
			msg = new FacesMessage(
				FacesMessage.SEVERITY_WARN,
				"Erro ao realizar login",
				"Login ou senha incorretos");
		}
		FacesContext.getCurrentInstance().addMessage(null, msg);
		context.addCallbackParam("logado", logado);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
