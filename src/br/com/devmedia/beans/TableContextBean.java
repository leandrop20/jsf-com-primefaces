package br.com.devmedia.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="tableContextBean")
@SessionScoped
public class TableContextBean {

	private List<Produto> produtos;
	private Produto produtoSelecionado;
	
	public TableContextBean() {
		produtos = new ArrayList<Produto>();
		
		produtos.add(new Produto("Laranja", 2.5f, 30));
		produtos.add(new Produto("Ma��", 1.8f, 30));
		produtos.add(new Produto("Uva", 1.5f, 30));
		produtos.add(new Produto("Mel�o", 3.7f, 30));
		produtos.add(new Produto("Banana", 1.3f, 30));
	}
	
	public void removerProduto() {
		produtos.remove(produtoSelecionado);
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}
	
}
