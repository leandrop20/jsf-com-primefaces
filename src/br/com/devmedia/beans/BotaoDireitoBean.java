package br.com.devmedia.beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ManagedBean(name="botaoDireitoBean")
@RequestScoped
public class BotaoDireitoBean {

	public void acessarWIFI(ActionEvent actionEvent) {
		adicionarMensagem("Conex�o realizada com sucesso ao WIFI");
	}
	
	public void salvar(ActionEvent actionEvent) {
		adicionarMensagem("Salvo com sucesso!");
	}
	
	public void adicionarMensagem(String mensagem) {
		FacesMessage msg = new FacesMessage(
			FacesMessage.SEVERITY_INFO, mensagem, null
		);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
}
