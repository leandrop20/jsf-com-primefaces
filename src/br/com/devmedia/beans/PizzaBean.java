package br.com.devmedia.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.chart.PieChartModel;

@ManagedBean(name="PizzaBean")
@RequestScoped
public class PizzaBean {

	private PieChartModel pizzaModel;
	
	public PizzaBean() {
		pizzaModel = new PieChartModel();
		
		pizzaModel.set("Candidato 1", 60);
		pizzaModel.set("Candidato 2", 30);
		pizzaModel.set("Candidato 3", 10);
		pizzaModel.set("Candidato 4", 10);
	}

	public PieChartModel getPizzaModel() {
		return pizzaModel;
	}
	
}