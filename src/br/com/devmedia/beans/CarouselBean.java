package br.com.devmedia.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="carouselBean")
@SessionScoped
public class CarouselBean {

	private List<Produto> produtos;
	private Produto produtoSelecionado;
	
	public CarouselBean() {
		produtos = new ArrayList<Produto>();
		
		produtos.add(new Produto("PS3", 999, 250));
		produtos.add(new Produto("XBOX", 799, 190));
		produtos.add(new Produto("WII", 699, 150));
		produtos.add(new Produto("iPhone", 45, 350));
		produtos.add(new Produto("Galaxy S3", 53, 262));
		produtos.add(new Produto("IPad 3", 1799, 290));
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}
	
}
