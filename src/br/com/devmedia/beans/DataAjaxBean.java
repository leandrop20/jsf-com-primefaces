package br.com.devmedia.beans;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.DateSelectEvent;

@ManagedBean(name="DataAjaxBean")
@RequestScoped
public class DataAjaxBean {

	private Date data;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public String printData() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		if (data != null) {
			String dt = sdf.format(data);
			System.out.println(dt);
			return dt;
		}
		return "-"; 
	}
	
	public void mostrarMsgMudancaData(DateSelectEvent event) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		facesContext.addMessage(
			null, 
			new FacesMessage(
				FacesMessage.SEVERITY_INFO, "Data Selecionada",
				sdf.format(event.getDate())
			)
		);
	}
	
}
