package br.com.devmedia.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="collectorBean")
@SessionScoped
public class CollectorBean {

	private Produto produto;
	private List<Produto> produtos;
	
	public CollectorBean() {
		produto = new Produto();
		produtos = new ArrayList<Produto>();
	}
	
	public String add() {
		produto = new Produto();
		return null;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	
}
