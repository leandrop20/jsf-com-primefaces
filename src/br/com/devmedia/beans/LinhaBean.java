package br.com.devmedia.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

@ManagedBean(name="linhaBean")
@RequestScoped
public class LinhaBean {

	private CartesianChartModel linhaModel;
	
	public LinhaBean() {
		linhaModel = new CartesianChartModel();
		
		ChartSeries canditato1 = new ChartSeries();
		canditato1.setLabel("Candidato 1");
		canditato1.set("Pesquisa 1", 10);
		canditato1.set("Pesquisa 2", 20);
		canditato1.set("Pesquisa 3", 40);
		canditato1.set("Pesquisa 4", 25);
		
		linhaModel.addSeries(canditato1);
		
		ChartSeries canditato2 = new ChartSeries();
		canditato2.setLabel("Candidato 2");
		canditato2.set("Pesquisa 1", 20);
		canditato2.set("Pesquisa 2", 30);
		canditato2.set("Pesquisa 3", 5);
		canditato2.set("Pesquisa 4", 70);
		
		linhaModel.addSeries(canditato2);
		
		ChartSeries canditato3 = new ChartSeries();
		canditato3.setLabel("Candidato 2");
		canditato3.set("Pesquisa 1", 35);
		canditato3.set("Pesquisa 2", 50);
		canditato3.set("Pesquisa 3", 10);
		canditato3.set("Pesquisa 4", 80);
		
		linhaModel.addSeries(canditato3);
	}

	public CartesianChartModel getLinhaModel() {
		return linhaModel;
	}
	
}
