package br.com.devmedia.beans;

public class Produto {

	private int id;
	private String nome;
	private float valor;
	private int totalVendas;
	private String descricao;
	private String categoria;
	
	public Produto() {
		
	}
	
	public Produto(int id, String nome, float valor, int totalVendas, String categoria) {
		this.id = id;
		this.nome = nome;
		this.valor = valor;
		this.totalVendas = totalVendas;
		this.categoria = categoria;
	}
	
	public Produto(String nome, float valor, int totalVendas) {
		this.nome = nome;
		this.valor = valor;
		this.totalVendas = totalVendas;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public float getValor() {
		return valor;
	}
	
	public void setValor(float valor) {
		this.valor = valor;
	}
	
	public int getTotalVendas() {
		return totalVendas;
	}
	
	public void setTotalVendas(int totalVendas) {
		this.totalVendas = totalVendas;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
